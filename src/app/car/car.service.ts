import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { CarModel } from './car.model';

@Injectable()
export class CarService {

  /**
   * The cars data read from the JSON file
   *
   * @var Map<string, CarModel>
   */
  private carData = new Map<string, CarModel>();

  /**
   * Subject to handle the readyness of the data
   *
   * @var BehaviorSubject<boolean>
   */
  private dataReady = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) {
    this.loadCarData();
  }

  /**
   * Read the cars data from the json file
   */
  private loadCarData() {
    this.http.get('assets/data/cars.json', {responseType: 'json'}).toPromise()
    .then(
      (cars: Array<CarModel>) => {
        for (const car of cars) {
          const model = new CarModel(car);
          this.carData.set(car.carId, model);
        }
        this.dataReady.next(true);
      }
    );
  }

  /**
   * Helper function to avoid using service methods until the data is ready to be used
   */
  private isReady() {
    return new Promise((resolve, reject) => {
      if (this.dataReady.getValue()) {
        resolve();
      } else {
        // If the data is not ready, wait to resolve until dataReady returns true
        const subscription = this.dataReady.subscribe((ready) => {
          if (ready) {
            subscription.unsubscribe();
            resolve();
          }
        });
      }
    });
  }

  /**
   * Retrieves the car with id carId
   *
   * @param carId string
   *    The id of the car to retrieve
   */
  public getCar(carId: string) {
    return this.isReady().then(() => {
      return this.carData.get(carId);
    });
  }

  /**
   * Retrieves the list of all cars
   */
  public getCars(sortBy = 'brand') {
    return this.isReady().then(() => {
      return Array.from(this.carData.values()).sort((a, b) => {
        return a[sortBy] > b[sortBy] ? 1 : -1;
      });
    });
  }
}
