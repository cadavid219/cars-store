import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { CarService } from './../car.service';
import { CarModel } from './../car.model';

@Component({
  selector: 'app-car-compare',
  templateUrl: './compare.component.html',
  styleUrls: ['./compare.component.scss']
})
export class CarCompareComponent implements OnInit, OnDestroy {

  /**
   * The subscription objects for the route params
   *
   * @var Subscription
   */
  private paramsSubscription: Subscription;

  /**
   * The list of cars to compare
   *
   * @var Array<CarModel>
   */
  public cars = new Array<CarModel>();

  constructor(
    private route: ActivatedRoute,
    private carService: CarService
  ) {}

  ngOnInit() {
    this.paramsSubscription  = this.route.queryParams.subscribe(
      (params: Params) => {
        for (const carId of Object.values(params)) {
          this.carService.getCar(carId).then((car) => {
            this.cars.push(car);
          });
        }
      }
    );
  }

  ngOnDestroy() {
    this.paramsSubscription.unsubscribe();
  }
}
