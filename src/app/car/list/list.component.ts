import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { CarModel } from './../car.model';
import { CarService } from './../car.service';

@Component({
  selector: 'app-car-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class CarListComponent implements OnInit {
  /**
   * The list of cars
   *
   * @var Array<CarModel>
   */
  public cars: Array<CarModel>;

  /**
   * Contains the current value the data is filtered by
   * (or empty if the data is not filtered)
   *
   * @var string
   */
  public filterBy = '';

  /**
   * The number of elements currently being displayed
   *
   * @var number
   */
  public elementCount = 0;

  /**
   * Indicates if the compare section must be shown or not
   *
   * @var boolean
   */
  public showCompareSection = false;

  /**
   * Contains the list of cars that will be sent to be compared
   *
   * @var new Array<string>
   */
  private comparableCars = new Array<string>();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public carService: CarService
  ) {}

  ngOnInit() {
    this.carService.getCars().then((cars) => {
      this.cars = cars;
      this.elementCount = this.cars.length;
    });
  }

  /**
   * Returns a class name for a car brand that is
   * used to filter the list of cars
   *
   * @param string carBrand
   *    The car brand to get the class for
   */
  private getBrandClass(carBrand: string) {
    return 'brand-' + carBrand.toLowerCase().replace(' ', '-');
  }

  /**
   * Event handler for the keyDown event of the search by brand input
   *
   * @param event
   *    The event object
   * @param searchInput
   *    The search input element
   */
  public onKeyDown(event, searchInput) {
    if (event.keyCode === 13) {
      this.onSearch(searchInput);
    }
  }

  /**
   * Filters the cars by the value of searchInput
   *
   * @param searchInput
   *    The search input element
   */
  public onSearch(searchInput) {
    if (searchInput.value === '') {
      this.filterBy     = '';
      this.elementCount = this.cars.length;
    } else {
      this.elementCount = 0;
      this.filterBy     = this.getBrandClass(searchInput.value);
      this.cars.forEach((car) => {
        this.elementCount += (car.brand.toLowerCase() === searchInput.value.toLowerCase()) ? 1 : 0;
      });
    }
  }

  /**
   * Handler for the click event on the compare button
   */
  public onCompare() {
    const itemsToCompare = this.comparableCars.length;
    if (itemsToCompare === 0) {
      this.showCompareSection = !this.showCompareSection;
    }
    if (!this.showCompareSection) {
      this.comparableCars = new Array<string>();
    } else if (itemsToCompare >= 1 && itemsToCompare <= 3) {
      const queryParams = {};
      for (let index = 0; index < this.comparableCars.length; index++) {
        queryParams['car' + index] = this.comparableCars[index];
      }
      this.router.navigate(['cars/compare'], {queryParams});
    }
  }

  /**
   * Handles the selection of a car to the compare list
   *
   * @param carId
   *    The id of the selected car to compare
   */
  public onSelectedItem(carId: string) {
    // If there are already 3 items remove the one that was added first
    // to give space to add the new one
    if (this.comparableCars.length === 3) {
      this.comparableCars.shift();
    }
    this.comparableCars.push(carId);
  }

  /**
   * Checks if an item can show the "Add to compare section"
   *
   * @param carId string
   *    The id of the car to perform the check for
   */
  public isCompareEnableForItem(carId: string) {
    return this.showCompareSection && !this.comparableCars.includes(carId);
  }
}

















