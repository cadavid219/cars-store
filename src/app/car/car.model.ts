/**
 * Interface for the picture elements
 */
export interface CarPictureModel {
  path: string;
  format: string;
  height: number;
  width: number;
}

/**
 * Object containing the data for a Car instance
 */
export class CarModel {
  public pictures: Array<CarPictureModel>;
  public features: Array<string>;
  public colors: Array<string>;
  public currency: string;
  public price: number;
  public description: string;
  public brand: string;
  public year: number;
  public model: string;
  public carId: string;

  constructor(car) {
    for (const property of Object.getOwnPropertyNames(car)) {
      this[property] = car[property];
    }
  }

  /**
   * Returns the largest image of the set belonging to this car
   * In this case the criteria to identify the largest is the width
   */
  public getLargestPicture() {
    return this.pictures.reduce((prev, curr) => {
      return (prev.width > curr.width) ? prev : curr;
    });
  }
}
