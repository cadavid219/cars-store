import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { CarService } from './../car.service';
import { CarModel } from './../car.model';

@Component({
  selector: 'app-car-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class CarDetailComponent implements OnInit, OnDestroy {

  /**
   * The subscription object for the route params
   */
  private paramsSubscription: Subscription;

  /**
   * The car object
   */
  public car: CarModel;

  constructor(
    private route: ActivatedRoute,
    private carService: CarService
  ) { }

  ngOnInit() {
    this.paramsSubscription  = this.route.params.subscribe(
      (params: Params) => {
        const carId = params['id'];
        this.carService.getCar(carId).then((car) => {
          this.car = car;
        });
      }
    );
  }

  ngOnDestroy() {
    this.paramsSubscription.unsubscribe();
  }
}
