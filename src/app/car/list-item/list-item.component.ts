import { Router } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { CarModel } from './../car.model';

@Component({
  selector: 'app-car-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class CarListItemComponent implements OnInit {

  /**
   * The car item
   *
   * @var car CarModel
   */
  @Input() public car: CarModel;

  /**
   * Index of the element in the grid
   *
   * @var index number
   */
  @Input() public index: number;

  /**
   * Checks if the "Add to compare" section should be visible
   *
   * @var boolean
   */
  @Input() public isCompareEnabled: boolean;

  /**
   * Emitter for the event of adding an item to the compare section
   *
   * @var EventEmitter<string>
   */
  @Output('SelectedItem') selectedItemEmitter = new EventEmitter<string>();

  constructor(private router: Router) { }

  ngOnInit() {}

  /**
   * Handler for the view details event on the item
   */
  public onViewDetails() {
    this.router.navigate(['cars', this.car.carId]);
  }

  /**
   * Handler for the add to compare event on the item
   */
  public onAddToCompare() {
    this.selectedItemEmitter.emit(this.car.carId);
  }
}
