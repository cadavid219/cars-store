import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { CarService } from './car/car.service';

import { AppComponent } from './app.component';
import { CarListComponent } from './car/list/list.component';
import { CarDetailComponent } from './car/detail/detail.component';
import { CarCompareComponent } from './car/compare/compare.component';
import { PageNotFoundComponent } from './errors/page-not-found/page-not-found.component';
import { CarListItemComponent } from './car/list-item/list-item.component';

const appRoutes: Routes = [
  {
    path: '',
    component: CarListComponent
  },
  {
    path: 'cars',
    component: CarListComponent,
  },
  {
    path: 'cars/compare',
    component: CarCompareComponent
  },
  {
    path: 'cars/:id',
    component: CarDetailComponent
  },
  {
    path: 'page-not-found',
    component: PageNotFoundComponent
  },
  {
    path: '**',
    redirectTo: '/page-not-found',
  },
];

@NgModule({
  declarations: [
    AppComponent,
    CarListComponent,
    CarListItemComponent,
    CarDetailComponent,
    CarCompareComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [CarService],
  bootstrap: [AppComponent]
})
export class AppModule { }
