# CarsStore

## Prerequisites
The main dependency is Angular CLI 1.7.0 which requires Node 6.9.0 or higher, together with NPM 3 or higher.

## Usage
1. Clone or download the project
2. cd to the projec's folder
3. Run `npm install`
4. Run `ng serve`
5. access the server that was created in the previous step (normally it is http://localhost:4200)

## Generate Data
A template file was created to generate dummy data to use in the app. To use it, update or simply copy the contents of the file `src\assets\data\carsTemplate.json` in https://next.json-generator.com, the data created there can then be saved in `src\assets\data\cars.json`,
which is the file from where the application reads the data
